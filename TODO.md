# Sampled Data Duration Todos #

- [x] Create first draft based on `audio-duration`.
- [x] Set up basic CI.
- [x] Add doc comments.
- [x] Create logo.
- [x] Update README.md headings and badges.
- [x] Release to crates.io
- [x] Add crates.io details to README.
- [x] Replace `audio-duration` use in other projects.
- [x] Deprecate `audio-duration`.
- [x] Implement basic subtraction for `ConstantRateDuration`.
- [x] Implement basic multiplication by a number of `MixedRateDuration`.
- [x] Implement basic division by a number of `MixedRateDuration`.
- [x] Implement basic subtraction for `MixedRateDuration`.
- [ ] Fix pedantic clippy warnings as much as possible.
- [ ] Consider a more meaningful alternative to types of the form
      `Result<ConstantRateDuration, MixedRateDuration`.
- [ ] Do a full code review.
