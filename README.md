<!-- markdownlint-disable MD033 -->
# <img src="doc/images/logo.png" width="100" alt="Logo"/>Sampled Data Duration

[![pipeline
status](https://gitlab.com/danieljrmay/sampled_data_duration/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/sampled_data_duration/commits/master)
[![Crate](https://img.shields.io/crates/v/sampled_data_duration.svg)](https://crates.io/crates/sampled_data_duration)
[![API](https://docs.rs/sampled_data_duration/badge.svg)](https://docs.rs/sampled_data_duration)

Rust library for handling durations of sampled data, e.g. digital
audio.

The `sampled_data_duration` crate provides two stucts:
`ConstantRateDuration` and `MixedRateDuration`.

A `ConstantRateDuraiton` can be used to represents the duration of any
data-set which has been sampled at a constant frequency, a prime
example might be an audio file sampled at 44.1kHz.

A `MixedRateDuration` can be used to represent the duration of a
collection of data-sets which have different sampling frequencies. A
typical example might be a playlist of audio files where some have
been sampled at 44.1kHz, and others at 48kHz or 96kHz, etc.

## Example

```rust
use sampled_data_duration::ConstantRateDuration;
use sampled_data_duration::MixedRateDuration;

// Consider an audio file which consists of `12345678` samples per
// channel recorded at a sampling rate of 44.1kHz.
let crd = ConstantRateDuration::new(12345678, 44100);

// The default string representation is of the form
// `hh:mm:ss;samples`
assert_eq!(crd.to_string(), "00:04:39;41778");

// Get the duration in various different time-units
assert_eq!(crd.as_hours(), 0);
assert_eq!(crd.as_mins(), 4);
assert_eq!(crd.submin_secs(), 39);
assert_eq!(crd.as_secs(), 4 * 60 + 39);
assert_eq!(crd.subsec_samples(), 41778);
assert_eq!(crd.subsec_secs(), 0.9473469387755102);

// Consider and audio playlist which already consists of a file
// recorded at 96kHz.
let mut mrd = MixedRateDuration::from(ConstantRateDuration::new(87654321, 96000));

// The default string representation of the a mixed rate duration
// which consits of only one entry is of the form `hh:mm:ss;samples`
assert_eq!(mrd.to_string(), "00:15:13;6321");

// However if we add-assign `crd` to `mrd`
mrd += crd;

// Then we have a duration which is made up of different sampling
// rates and the default string representation changes to be of the
// form `hh:mm:ss.s`
assert_eq!(mrd.to_string(), "00:19:53.013190688");
```

An attempt has been made to follow the naming conventions defined by
[`std::time::Duration`](https://doc.rust-lang.org/std/time/struct.Duration.html).

## Installation

You can obtain the latest release of this library from
[crates.io](https://crates.io/crates/sampled_data_duration) in the
normal way.

## API Documentation

You can read the API documentation for the latest release of this
library from
[docs.rs](https://docs.rs/sampled_data_duration).
